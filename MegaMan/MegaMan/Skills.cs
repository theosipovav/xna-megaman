﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace MegaMan
{
    class Skills
    {
        protected Texture2D Texture; // Тектура
        protected Rectangle Rectangle; // Ректангл для отрисовки
        protected Vector2 Position; // Позиция
        protected float Rotation;
        protected Color Colour;
        // Открытые
        public Rectangle Rectangle_Interaction; // Ректангл для взаимодейсвия

        public Skills(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            Rotation = 0f;
            Colour = Color.White;
        }

        public virtual void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            _SpriteBatch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, Texture.Height), Colour, 0f, new Vector2(Texture.Width / 2, Texture.Height / 2), 1.0f, SpriteEffects.None, newDepth);
        }

    }
    class Rocket : Skills
    {
        private Vector2 Original_position, StartPosition;
        private int FrameHeight, FrameWidth, FrameCurrent, Range;
        private float TimerFrame;
        private string Route;
        // Открытые
        public bool FlagUse;

        public Rocket(Texture2D newTexture, Vector2 newPosition) : base(newTexture, newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            FrameHeight = 28;
            FrameWidth = 60;
            FrameCurrent = 0;
            FlagUse = false;
            Range = 500;
        }

        public void Update(GameTime _GameTime)
        {
            if (FlagUse)
            {
                Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
                Rectangle_Interaction = new Rectangle((int)Position.X - 30, (int)Position.Y - 14, 60, 28);
                Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
                TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (TimerFrame >= 250)
                {
                    if (FrameCurrent == 0) FrameCurrent = 1;
                    else FrameCurrent = 0;
                    TimerFrame = 0;
                }
                // Движение вправо пока не достигнет отметке Range
                if (Route == "A1")
                {
                    if (Position.X - StartPosition.X < Range) Position.X += 10;
                    else FlagUse = false;
                }
                // Движение влево
                if (Route == "A2")
                {
                    if (StartPosition.X - Position.X < Range) Position.X -= 10;
                    else FlagUse = false;
                }
            }
        }
        public new void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            if (FlagUse)
            {
                if (Route == "A1") _SpriteBatch.Draw(Texture, Position, Rectangle, Color.White, 0f, Original_position, 1.0f, SpriteEffects.None, 0.5f);
                if (Route == "A2") _SpriteBatch.Draw(Texture, Position, Rectangle, Color.White, 0f, Original_position, 1.0f, SpriteEffects.FlipHorizontally, 0.5f);
            }
        }
        public void Attack(Vector2 newPosition, string newRoute)
        {
            if (!FlagUse)
            {
                Position = newPosition;
                StartPosition = newPosition;
                Route = newRoute;
                FlagUse = true;
            }
        }
    }
}
