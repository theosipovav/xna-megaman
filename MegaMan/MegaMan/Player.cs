﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MegaMan
{
    class Player
    {
        private Texture2D Texture;
        private Rectangle Rectangle;
        private Vector2 Original_position, Velosity;
        private KeyboardState KS;
        private Color Colour;
        private int FrameHeight, FrameWidth, FrameCurrent;
        private float TimerFrame, TimerImmunity, TimerJump;
        private bool FlagJumpUp;
        public bool FlagJumpDown;

        // Открытые
        public Vector2 Position;
        public Rectangle Rectangle_Interaction;
        public string Name = "Игрок", PlayerRoute, Status, AttackRoute;
        public int HP = 3, XP = 0;
        public int Speed = 6;
        public bool FlagImmunity, flagAttack;

        public Player(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            FrameHeight = 61;
            FrameWidth = 70;
            PlayerRoute = "C1";
            Status = "Live";
            AttackRoute = "";
            FlagImmunity = FlagJumpUp = FlagJumpDown = flagAttack = false;
            TimerFrame = TimerImmunity = TimerJump = 0;
            Colour = new Color(255, 255, 255, 255);
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            TimerJump += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (HP <= 0) Status = "Dead";

            if (flagAttack)
            {
                switch (AttackRoute)
                {
                    case "A1":
                        FrameCurrent = 18;
                        TimerFrame = 0;
                        break;
                    case "A2":
                        FrameCurrent = 19;
                        TimerFrame = 0;
                        break;
                }
            }
            else
            {

                switch (PlayerRoute)
                {
                    case "C1":
                        FrameCurrent = 0;
                        break;
                    case "C2":
                        FrameCurrent = 17;
                        break;
                    case "L":
                        if (TimerFrame >= 100)
                        {
                            if (FrameCurrent > 9) FrameCurrent--;
                            else FrameCurrent = 16;
                            TimerFrame = 0;
                        }
                        break;
                    case "R":
                        if (TimerFrame >= 100)
                        {
                            if (FrameCurrent < 8) FrameCurrent++;
                            else FrameCurrent = 1;
                            TimerFrame = 0;
                        }
                        break;
                    default:
                        break;
                }
                if (FlagJumpUp)
                {
                    if (TimerJump > 1)
                    {
                        if (Position.Y > 150)
                        {
                            Position.Y = Position.Y - 16;
                            TimerJump = 0;
                        }
                        else
                        {
                            FlagJumpUp = false;
                            TimerJump = 0;
                        }
                    }
                }
            }
            // Управление:
            Control(_GameTime);
            // Имуннетет от урона
            if (FlagImmunity)
            {
                TimerImmunity += (float)_GameTime.ElapsedGameTime.TotalSeconds;
                Colour = Color.IndianRed;
                if (TimerImmunity >= 3)
                {
                    TimerImmunity = 0;
                    FlagImmunity = false;
                    Colour = Color.White;
                }
            }
        }

        public void Draw(SpriteBatch _SpriteBatch, SpriteFont _SpriteFont)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.5f);
        }
        private void Control(GameTime _GameTime)
        {
            KS = Keyboard.GetState();
            if (!flagAttack)
            {
                if (KS.IsKeyDown(Keys.Left))
                {
                    PlayerRoute = "L";
                    Velosity.X = -Speed;
                    Position.X = Position.X + Velosity.X;
                }
                else
                {
                    if (KS.IsKeyDown(Keys.Right))
                    {
                        PlayerRoute = "R";
                        Velosity.X = Speed;
                        Position.X = Position.X + Velosity.X;
                    }
                    else
                    {
                        if (PlayerRoute == "L") PlayerRoute = "C2";
                        if (PlayerRoute == "R") PlayerRoute = "C1";
                    }
                }
                if ((KS.IsKeyDown(Keys.Space)) && (FlagJumpUp == false) && (FlagJumpDown == true))
                {
                    FlagJumpUp = true;
                    FlagJumpDown = false;
                }
            }
            if (KS.IsKeyDown(Keys.W))
            {
                flagAttack = true;
                if ((PlayerRoute == "C1") || ((PlayerRoute == "R")))
                    AttackRoute = "A1";
                if ((PlayerRoute == "C2")||(PlayerRoute == "L"))
                    AttackRoute = "A2";
                FlagJumpUp = false;
                TimerJump = 0;
            }
            else
            {
                flagAttack = false;
            }
        }
    }
}
