﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MegaMan
{
    class Objects
    {
        protected Texture2D Texture;
        protected Rectangle Rectangle;
        protected Vector2 Original_position;
        protected float Timer, TimerFrame;
        protected int FrameHeight, FrameWidth, FrameCurrent;
        public Rectangle Rectangle_Interaction;
        public Vector2 Position;
        public Color Colour;
        public Objects(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            Colour = new Color(255, 255, 255, 255);
            FrameCurrent = 0;
            Timer = 0;

        }
        public virtual void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, newDepth);
        }
    }
    class Platform : Objects
    {
        public Platform(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            FrameHeight = newTexture.Height;
            FrameWidth = newTexture.Width;
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2();
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight);
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight);

        }
    }

    class Portal : Objects
    {
        public bool flagUse;
        public Portal(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            FrameHeight = newTexture.Height;
            FrameWidth = newTexture.Width;
            flagUse = false;
        }
        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2();
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight);
            Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight);
        }

        public virtual void Draw1(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, newDepth);
        }
    }
    class Points : Objects
    {
        public bool FlagUse;
        public Points(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            FrameHeight = FrameWidth = 33;
        }
        public void Update(GameTime _GameTime)
        {
            if (!FlagUse)
            {
                Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
                Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
                Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
                TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (TimerFrame >= 500)
                {
                    if (FrameCurrent == 0) FrameCurrent = 1;
                    else FrameCurrent = 0;
                    TimerFrame = 0;
                }
            }
        }
        public new void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            if (!FlagUse) _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.51f);
        }
        public void Delete()
        {
            Position = new Vector2(-1000, -1000);
            Rectangle = Rectangle_Interaction = new Rectangle((int)Position.X, (int)Position.Y, 0, 0);
            FlagUse = true;
        }
        public void NewPositionRND(Random newRND, int A = 200, int B = 5000)
        {
            Position = new Vector2(newRND.Next(A, B), newRND.Next(150, 400));
        }
    }
    class Enemies : Objects
    {
        private float TimerPosition, SavePosition;
        private bool FP;
        public bool _flagLive;
        public Enemies(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            SavePosition = newPosition.X;
            FrameHeight = FrameWidth = 61;
            TimerPosition = 0;
            FP = true;
            _flagLive = true;
        }
        public void Update(GameTime _GameTime)
        {

            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            if (_flagLive)
            {
                TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                TimerPosition += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (TimerFrame >= 500)
                {
                    if (FrameCurrent == 0) FrameCurrent = 1;
                    else FrameCurrent = 0;
                    TimerFrame = 0;
                }
                if (TimerPosition >= 500)
                {
                    if (FP)
                    {
                        Position.X += 3;
                        if ((Position.X - SavePosition) >= 300) FP = false;
                    }
                    if (!FP)
                    {
                        Position.X -= 3;
                        if ((Position.X - SavePosition) <= 0) FP = true;
                    }
                }
            }
        }

    }
    class Boss : Objects
    {
        private float TimerPosition, SavePosition, TimerImmunity;
        private bool FP, FlagImmunity;
        public Vector2 Velosity;
        public int HP;
        public bool _flagLive;
        public Boss(Texture2D newTexture, Vector2 newPosition, float newLayerDepth = 0.3f, int newModWidth = 0, int newModHeight = 0) : base(newTexture, newPosition)
        {
            SavePosition = newPosition.X;
            Velosity = new Vector2(3, 3);
            FrameHeight = 80;
            FrameWidth = 376 / 2;
            TimerPosition = 0;
            TimerImmunity = 0;
            HP = 5;
            FP = true;
            FlagImmunity = false;
            _flagLive = true;
        }
        public void Update(GameTime _GameTime)
        {

            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            if (_flagLive)
            {
                TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                TimerPosition += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                if (TimerFrame >= 500)
                {
                    if (FrameCurrent == 0) FrameCurrent = 1;
                    else FrameCurrent = 0;
                    TimerFrame = 0;
                }
                if (Position.Y <= 50) Velosity.Y = -Velosity.Y;
                if (Position.Y >= 530) Velosity.Y = -Velosity.Y;
                if (Position.X <= 150) Velosity.X = -Velosity.X;
                if (Position.X >= 1500) Velosity.X = -Velosity.X;
                Position += Velosity;
                if (FlagImmunity)
                {
                    TimerImmunity += (float)_GameTime.ElapsedGameTime.TotalSeconds;
                    if (TimerImmunity >= 3)
                    {
                        TimerImmunity = 0;
                        FlagImmunity = false;
                        Colour = Color.White;
                    }
                }

            }
        }
        public void Damage()
        {
            if (!FlagImmunity)
            {
                HP -= 1;
                Colour = Color.Red;
                FlagImmunity = true;
            }
        }
    }
}
