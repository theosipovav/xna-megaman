using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml;
using System.IO;

namespace MegaMan
{

    public class Game : Microsoft.Xna.Framework.Game
    {
        /* ���������� ���������� ������� */
        GraphicsDeviceManager _GraphicsDeviceManager;
        SpriteBatch _SpriteBatch;
        KeyboardState _KeyUP, _KeyDown;
        Viewport _ViewportUI, _ViewportGame, _ViewportMenu, _ViewportOriginal;
        Random RND;

        /* ���������� ������� ��� ���� */
        Buttons[] _Buttons = new Buttons[4];
        InputText _InputText;

        /* ���������� ������� ��� ���� ������� */
        Portal _Portal;
        Camera _Camera;
        Player _Player;
        UI _UI;
        Background[] _BackgroundLvL = new Background[2];
        Rocket _Rocket;
        Song[] Sound = new Song[2];
        bool[] FlagStart = new bool[2];

        /* ___���������� ������� ��� 1 ������ */
        List<Platform> _PlatformLvl1 = new List<Platform>();
        List<Points> _Points = new List<Points>();
        List<Enemies> _EnemiesLvl1 = new List<Enemies>();
        int _SecTime;



        float _FallingTime, _Time;
        bool _FlagFalling, _FlagFallingP;

        /* ___���������� ������� ��� 2 ������ */
        List<Platform> _PlatformLvl2 = new List<Platform>();
        List<Enemies> _EnemiesLvl2 = new List<Enemies>();
        Boss _Boss;


        /* ���������� �������� ���� */
        enum Structure_Game
        {
            Menu,
            Play,
            Final,
        }
        Structure_Game CurrentGame = Structure_Game.Menu;
        enum Structure_Menu
        {
            Global,
            Rating,
        }
        Structure_Menu CurrentMenu = Structure_Menu.Global;
        enum Structure_Play
        {
            LVL1,
            LVL2,
        }
        Structure_Play CurrentPlay = Structure_Play.LVL1;

        public Game()
        {
            _GraphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

        }


        protected override void Initialize()
        {
            _ViewportMenu = new Viewport();
            _ViewportMenu.X = 0;
            _ViewportMenu.Y = 0;
            _ViewportMenu.Width = 1024;
            _ViewportMenu.Height = 600;
            _ViewportMenu.MinDepth = 0;
            _ViewportMenu.MaxDepth = 1;
            _ViewportUI = new Viewport();
            _ViewportUI.X = 0;
            _ViewportUI.Y = 0;
            _ViewportUI.Width = 1024;
            _ViewportUI.Height = 50;
            _ViewportUI.MinDepth = 0;
            _ViewportUI.MaxDepth = 1;
            _ViewportGame = new Viewport();
            _ViewportGame.X = 0;
            _ViewportGame.Y = 50;
            _ViewportGame.Width = 1024;
            _ViewportGame.Height = 550;
            _ViewportGame.MinDepth = 0;
            _ViewportGame.MaxDepth = 1;
            _Camera = new Camera();
            base.Initialize();
        }


        protected override void LoadContent()
        {
            /* ������������� ���������� ������� */
            _SpriteBatch = new SpriteBatch(GraphicsDevice);
            IsMouseVisible = true;
            _GraphicsDeviceManager.PreferredBackBufferWidth = 1024;
            _GraphicsDeviceManager.PreferredBackBufferHeight = 600;
            _GraphicsDeviceManager.ApplyChanges();

            /* ������������� ������� ��� ���� */
            _Buttons[0] = new Buttons(Content.Load<Texture2D>("ButtonStart"), new Vector2(150, 550));
            _Buttons[1] = new Buttons(Content.Load<Texture2D>("ButtonRating"), new Vector2(1024 - 450, 550));
            _Buttons[2] = new Buttons(Content.Load<Texture2D>("ButtonExit"), new Vector2(1024 - 150, 550));
            _Buttons[3] = new Buttons(Content.Load<Texture2D>("ButtonCancel"), new Vector2(850, 45));
            _InputText = new InputText(new Vector2(150, 275), Content.Load<SpriteFont>("InputText"));

            /* ������������� ������� ��� ���� ������� */
            _Player = new Player(Content.Load<Texture2D>("Player"), new Vector2(150, 350));
            _UI = new UI(Content.Load<Texture2D>("UI"), Content.Load<Texture2D>("CountHP"), new Vector2(0, 0), 0, 0, Content.Load<SpriteFont>("TextUI"));
            _Rocket = new Rocket(Content.Load<Texture2D>("Skill"), new Vector2(100, 300));
            _Portal = new Portal((Content.Load<Texture2D>("Portal")), new Vector2(100 + 140 * 36, 410));
            RND = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            Sound[0] = Content.Load<Song>("SoundMenu");
            Sound[1] = Content.Load<Song>("SoundPlay");
            FlagStart[0] = false;
            FlagStart[1] = false;

            /* ������������� ������� ��� 1 ������ */
            _BackgroundLvL[0] = new Background(Content.Load<Texture2D>("BackgroundLvL1"));
            if (_PlatformLvl1.Count > 0) _PlatformLvl1.Clear();
            for (int n = 0; n < 35; n++) 
            {
                if (n == 6) continue;
                if (n == 8) continue;
                if (n == 11) continue;
                if (n == 12) continue;
                if (n == 17) continue;
                if (n == 21) continue;
                if (n == 22) continue;
                if (n == 27) continue;
                if (n == 28) continue;
                _PlatformLvl1.Add(new Platform(Content.Load<Texture2D>("Platforma"), new Vector2(100 + 140 * n, 410)));
            }
            if (_Points.Count > 0) _Points.Clear();
            for (int i = 0; i < 50; i++)
            {
                _Points.Add(new Points(Content.Load<Texture2D>("Points"), new Vector2(0, 0)));
                _Points[i].NewPositionRND(RND);
            }
            if (_EnemiesLvl1.Count > 0) _EnemiesLvl1.Clear();
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(600, 370)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(1950, 370)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(2650, 370)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(3380, 370)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(4450, 370)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(2850, 170)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(3000, 170)));
            _EnemiesLvl1.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(3850, 170)));
            _SecTime = 30;
            _Time = 0;

            /* ������������� ������� ��� 2 ������ */
            _BackgroundLvL[1] = new Background(Content.Load<Texture2D>("BackgroundLvL2"));
            _Boss = new Boss(Content.Load<Texture2D>("Boss"), new Vector2(500, 350));
            if (_PlatformLvl2.Count > 0) _PlatformLvl2.Clear();
            for (int n = 0; n < 10; n++)
            {
                _PlatformLvl2.Add(new Platform(Content.Load<Texture2D>("Platforma"), new Vector2(100 + 140 * n, 410)));
            }
            if (_EnemiesLvl2.Count > 0) _EnemiesLvl2.Clear();
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(500, 370)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(550, 320)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(500, 270)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(550, 220)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(500, 170)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(800, 370)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(850, 320)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(800, 270)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(850, 220)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(800, 170)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(1200, 370)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(2150, 320)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(1200, 270)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(1250, 220)));
            _EnemiesLvl2.Add(new Enemies(Content.Load<Texture2D>("Enemies"), new Vector2(1200, 170)));

            _FallingTime = 0;
            _FlagFalling = true;
            _FlagFallingP = false;

        }


        protected override void UnloadContent()
        {

        }


        protected override void Update(GameTime _GameTime)
        {
            MouseState _MouseState = Mouse.GetState();
            _KeyUP = _KeyDown;
            _KeyDown = Keyboard.GetState();


            switch (CurrentGame)
            {
                case Structure_Game.Menu:
                    switch (CurrentMenu)
                    {
                        case Structure_Menu.Global:
                            if (FlagStart[0] == false)
                            {
                                MediaPlayer.Play(Sound[0]);
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Volume = 0.3f;
                                FlagStart[0] = true;
                            }
                            for (int n = 0; n < 3; n++) _Buttons[n].Update(_MouseState);
                            if (_Buttons[1].isClicked == true) CurrentMenu = Structure_Menu.Rating;
                            if (_Buttons[2].isClicked == true) Exit();
                            if (_Buttons[0].isClicked == true)
                            {
                                if ((_InputText.FullText.Length <= 7) && (_InputText.FullText.Length > 0))
                                {
                                    _Player.Name = _InputText.FullText;
                                    CurrentGame = Structure_Game.Play;
                                    CurrentPlay = Structure_Play.LVL1;
                                }
                            }
                            _InputText.Update(_GameTime);
                            break;
                        case Structure_Menu.Rating:
                            _Buttons[3].Update(_MouseState);
                            if (_Buttons[3].isClicked == true) CurrentMenu = Structure_Menu.Global;
                            break;
                    }
                    break;
                case Structure_Game.Play:
                    if (FlagStart[1] == false)
                    {
                        MediaPlayer.Play(Sound[1]);
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Volume = 0.3f;
                        FlagStart[1] = true;
                    }
                    _Player.Update(_GameTime);
                    _UI = new UI(Content.Load<Texture2D>("UI"), Content.Load<Texture2D>("CountHP"), new Vector2(0, 0), 0, 0, Content.Load<SpriteFont>("TextUI"));
                    _UI.Update(_GameTime, _Player.Name, _Player.HP, _Player.XP, _SecTime);
                    _Camera.Update(_Player.Position);
                    if (_Player.Status == "Dead") CurrentGame = Structure_Game.Final;
                    _Rocket.Update(_GameTime);
                    /* ������ ������� */
                    if (_FlagFalling)
                    {
                        _FallingTime += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
                        if (_FallingTime > 1)
                        {
                            _Player.Position.Y += 8;
                            _FallingTime = 0;
                        }
                    }
                    _FlagFallingP = false;
                    
                    /* ������ �� ������� */
                    if (_Player.Position.Y > 650)
                    {
                        _Player.Position = new Vector2(150, 300);
                        if (!_Player.FlagImmunity)
                        {
                            _Player.HP--;
                            _Player.FlagImmunity = true;
                        }
                    }

                    /* �������� */
                    _KeyUP = _KeyDown;
                    _KeyDown = Keyboard.GetState();

                    if (_Player.flagAttack)
                    {
                        _Rocket.Attack(_Player.Position, _Player.AttackRoute);
                    }
                    switch (CurrentPlay)
                    {
                        case Structure_Play.LVL1:
                            /* ������ ��� ������ */
                            _Time += (float)_GameTime.ElapsedGameTime.TotalSeconds;
                            if (_Time > 1)
                            {
                                _SecTime--;
                                _Time = 0;
                            }
                            if (_SecTime <= 0) _Player.Status = "Dead";
                               
                            /* ���������� ������ ������ */
                            if (_Player.Position.X <= 150) _Player.Position.X = _Player.Position.X + _Player.Speed;
                            if (_Player.Position.X >= 5500) _Player.Position.X = _Player.Position.X - _Player.Speed;

                            /* ���������� �������� */
                            for (int i = 0; i < _PlatformLvl1.Count; i++)
                            {
                                _PlatformLvl1[i].Update(_GameTime);
                                if ((_Player.Rectangle_Interaction.Bottom >= _PlatformLvl1[i].Rectangle_Interaction.Top) &&
                                    (_Player.Rectangle_Interaction.Bottom <= _PlatformLvl1[i].Rectangle_Interaction.Bottom) &&
                                    (_Player.Rectangle_Interaction.Right >= _PlatformLvl1[i].Rectangle_Interaction.Left) &&
                                    (_Player.Rectangle_Interaction.Left <= _PlatformLvl1[i].Rectangle_Interaction.Right))
                                {
                                    _FlagFalling = false;
                                    _FlagFallingP = true;
                                    _Player.FlagJumpDown = true;
                                }
                                else
                                {
                                    if (!_FlagFallingP)
                                    {
                                        _FlagFalling = true;
                                        _Player.FlagJumpDown = false;
                                    }
                                }
                            }

                            /* ���������� ����� */
                            for (int i = 0; i < _Points.Count; i++)
                            {
                                _Points[i].Update(_GameTime);
                                if (_Points[i].Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                                {
                                    _Player.XP++;
                                    _Points[i].FlagUse = true;
                                    _Points[i].Delete();
                                }
                            }

                            /* ���������� ������ */
                            for (int i = 0; i < _EnemiesLvl1.Count; i++)
                            {
                                _EnemiesLvl1[i].Update(_GameTime);
                                if (_EnemiesLvl1[i].Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                                {
                                    if (!_Player.FlagImmunity)
                                    {
                                        _Player.HP--;
                                        _Player.FlagImmunity = true;
                                    }
                                }
                                if (_EnemiesLvl1[i].Rectangle_Interaction.Intersects(_Rocket.Rectangle_Interaction))
                                {
                                    _EnemiesLvl1[i]._flagLive = false;
                                    _EnemiesLvl1[i].Position.Y = 500;
                                    _EnemiesLvl1[i].Colour = Color.Red;

                                }
                            }

                            /* ���������� �������� �� 2 ������� */
                            _Portal.Update(_GameTime);
                            if (_Portal.Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                            {
                                CurrentPlay = Structure_Play.LVL2;
                                _Player.Position = new Vector2(150, 300);
                                if ((_Player.XP >= 10) && (_Player.XP <= 19)) _Player.HP += 1;
                                if ((_Player.XP >= 20) && (_Player.XP <= 29)) _Player.HP += 2;
                                if ((_Player.XP >= 30) && (_Player.XP <= 39)) _Player.HP += 3;
                                if ((_Player.XP >= 40) && (_Player.XP <= 49)) _Player.HP += 4;
                                if (_Player.XP >= 150) _Player.HP += 5;
                            }
                            break;
                        case Structure_Play.LVL2:
                            /* ���������� ������ ������ */
                            if (_Player.Position.X <= 150) _Player.Position.X = _Player.Position.X + _Player.Speed;
                            if (_Player.Position.X >= 5500) _Player.Position.X = _Player.Position.X - _Player.Speed;

                            /* ���������� ����� */
                            _Boss.Update(_GameTime);
                            if (_Boss.Rectangle_Interaction.Intersects(_Rocket.Rectangle_Interaction)) _Boss.Damage();
                            if (_Boss.HP <= 0) CurrentGame = Structure_Game.Final;
                            if (_Boss.Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                            {
                                if (!_Player.FlagImmunity)
                                {
                                    _Player.HP--;
                                    _Player.FlagImmunity = true;
                                }
                            }


                            /* ���������� ������ */
                            for (int i = 0; i < _EnemiesLvl2.Count; i++)
                            {
                                _EnemiesLvl2[i].Update(_GameTime);
                                if (_EnemiesLvl2[i].Rectangle_Interaction.Intersects(_Player.Rectangle_Interaction))
                                {
                                    if (!_Player.FlagImmunity)
                                    {
                                        _Player.HP--;
                                        _Player.FlagImmunity = true;
                                    }
                                }
                                if (_EnemiesLvl2[i].Rectangle_Interaction.Intersects(_Rocket.Rectangle_Interaction))
                                {
                                    _EnemiesLvl2[i]._flagLive = false;
                                    _EnemiesLvl2[i].Position.Y = 500;
                                    _EnemiesLvl2[i].Colour = Color.Red;

                                }
                            }

                            /* ���������� �������� */
                            for (int i = 0; i < _PlatformLvl2.Count; i++)
                            {
                                _PlatformLvl2[i].Update(_GameTime);
                                if ((_Player.Rectangle_Interaction.Bottom >= _PlatformLvl2[i].Rectangle_Interaction.Top) &&
                                    (_Player.Rectangle_Interaction.Bottom <= _PlatformLvl2[i].Rectangle_Interaction.Bottom) &&
                                    (_Player.Rectangle_Interaction.Right >= _PlatformLvl2[i].Rectangle_Interaction.Left) &&
                                    (_Player.Rectangle_Interaction.Left <= _PlatformLvl2[i].Rectangle_Interaction.Right))
                                {
                                    _FlagFalling = false;
                                    _FlagFallingP = true;
                                    _Player.FlagJumpDown = true;
                                }
                                else
                                {
                                    if (!_FlagFallingP)
                                    {
                                        _FlagFalling = true;
                                        _Player.FlagJumpDown = false;
                                    }
                                }
                            }
                            break;
                    }
                    break;
                case Structure_Game.Final:
                    switch (_Player.Status)
                    {
                        case "Live":
                            if (_KeyDown.IsKeyDown(Keys.Enter) && _KeyUP.IsKeyUp(Keys.Enter))
                            {
                                CurrentGame = Structure_Game.Menu;
                                SaveResult(_Player.Name, _Player.XP);
                                LoadContent();
                            }
                            break;
                        case "Dead":
                            if (_KeyDown.IsKeyDown(Keys.Enter) && _KeyUP.IsKeyUp(Keys.Enter))
                            {
                                CurrentGame = Structure_Game.Menu;
                                SaveResult(_Player.Name, _Player.XP);
                                LoadContent();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
            }

            base.Update(_GameTime);
        }


        protected override void Draw(GameTime _GameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            MouseState _MouseState = Mouse.GetState();
            switch (CurrentGame)
            {
                case Structure_Game.Menu:
                    _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);
                    switch (CurrentMenu)
                    {
                        case Structure_Menu.Global:
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundMenu"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            for (int n = 0; n < 3; n++) _Buttons[n].Draw(_SpriteBatch);
                            _InputText.Draw(_SpriteBatch);
                            break;
                        case Structure_Menu.Rating:
                            LoadResult();
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundRating"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            _Buttons[3].Draw(_SpriteBatch);
                            break;
                    }
                    _SpriteBatch.End();
                    break;
                case Structure_Game.Play:
                    // ��������� UI
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportUI;
                    _SpriteBatch.Begin();
                    _UI.Draw(_SpriteBatch);
                    _SpriteBatch.End();
                    // ��������� �������
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportGame;
                    _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, _Camera._MatrixTransform);
                    _Player.Draw(_SpriteBatch, Content.Load<SpriteFont>("InputText"));
                    _Rocket.Draw(_SpriteBatch);

                    switch (CurrentPlay)
                    {
                        case Structure_Play.LVL1:
                            _BackgroundLvL[0].Draw(_SpriteBatch);
                            for (int i = 0; i < _PlatformLvl1.Count; i++) _PlatformLvl1[i].Draw(_SpriteBatch);
                            for (int i = 0; i < _Points.Count; i++) _Points[i].Draw(_SpriteBatch);
                            for (int i = 0; i < _EnemiesLvl1.Count; i++) _EnemiesLvl1[i].Draw(_SpriteBatch, 0.509f);
                            _Portal.Draw(_SpriteBatch);
                            break;
                        case Structure_Play.LVL2:
                            _BackgroundLvL[1].Draw(_SpriteBatch);
                            _Boss.Draw(_SpriteBatch);
                            for (int i = 0; i < _PlatformLvl2.Count; i++) _PlatformLvl2[i].Draw(_SpriteBatch);
                            for (int i = 0; i < _EnemiesLvl2.Count; i++) _EnemiesLvl2[i].Draw(_SpriteBatch, 0.509f);
                            break;
                    }
                    _SpriteBatch.End();
                    break;
                case Structure_Game.Final:
                    _GraphicsDeviceManager.GraphicsDevice.Viewport = _ViewportMenu;
                    _SpriteBatch.Begin();
                    switch (_Player.Status)
                    {
                        case "Live":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundEndWin"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            break;
                        case "Dead":
                            _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundEndFail"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            break;
                        default:
                            break;
                    }
                    _SpriteBatch.End();
                    break;
            }
            base.Draw(_GameTime);
        }

        private void SaveResult(string _PlayerName, int _PlayerXP)
        {
            string _PlayerDate = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string _CurrentDirectory = Environment.CurrentDirectory;
            FileStream _FileStream;
            XmlDocument _XmlDocument;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Create);
                if (_XmlDocument["Game"].ChildNodes.Count <= 5)
                {
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                else
                {
                    _XmlDocument["Game"].RemoveAll();
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
            else
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.CreateNew);
                _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml("<Game></Game>");
                XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                _XmlAttribute1.Value = _PlayerName;
                _XmlNode.Attributes.Append(_XmlAttribute1);
                XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                _XmlAttribute2.Value = _PlayerXP.ToString();
                _XmlNode.Attributes.Append(_XmlAttribute2);
                XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                _XmlAttribute3.Value = _PlayerDate;
                _XmlNode.Attributes.Append(_XmlAttribute3);
                _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }
        private void LoadResult()
        {
            string _CurrentDirectory = Environment.CurrentDirectory;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                string _PlayerName = "";
                string _PlayerXP = "";
                string _PlayerDate = "";
                int _interval = 50;
                FileStream _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                for (int i = 0; i < _XmlDocument["Game"].ChildNodes.Count; i++)
                {
                    _PlayerName = _XmlDocument["Game"].ChildNodes[i].Attributes["Name"].InnerText;
                    _PlayerXP = _XmlDocument["Game"].ChildNodes[i].Attributes["XP"].InnerText;
                    _PlayerDate = _XmlDocument["Game"].ChildNodes[i].Attributes["Date"].InnerText;
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerName, new Vector2(100, 150 + _interval), Color.White);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerXP, new Vector2(450, 150 + _interval), Color.White);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextUI"), _PlayerDate, new Vector2(700, 150 + _interval), Color.White);
                    _interval += 50;
                }
            }
        }
    }
}
