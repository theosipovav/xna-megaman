﻿using Microsoft.Xna.Framework;

namespace MegaMan
{
    class Camera
    {
        public Vector2 _Position;
        public Matrix _MatrixTransform;
        public void Update(Vector2 _InPlayerPosition)
        {
            _Position.X = _InPlayerPosition.X - (GraphicsDeviceManager.DefaultBackBufferWidth / 2);
            if (_Position.X < 0) _Position.X = 0;
            _MatrixTransform = Matrix.CreateTranslation(new Vector3(-_Position, 0));
        }
    }
}
