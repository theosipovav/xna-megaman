﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MegaMan
{
    class Background
    {
        private Texture2D _Texture;
        private Rectangle[] _Rectangle = new Rectangle[9];

        public Background(Texture2D _InTexture)
        {
            int _PushX = 0;
            for (int i = 0; i < 9; i++)
            {
                _Rectangle[i] = new Rectangle(_PushX, 0, 1024, 600);
                _PushX += 1024;
            }
            _Texture = _InTexture;
        }

        public void Draw(SpriteBatch _SpriteBatch)
        {
            for (int i = 0; i < 9; i++)
            {
                _SpriteBatch.Draw(_Texture, _Rectangle[i], null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
            }
        }
    }
}
